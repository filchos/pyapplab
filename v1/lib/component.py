import importlib
import json
import os
import sys


class FlaskBlueprintOverwriter:
    def __init__(self, app, id):
        self.app = app
        self.id = id

    def __enter__(self):
        # allow to use Blueprint setupmethods after Flask already handled a route
        self._check_setup_finished = self.app._check_setup_finished
        self.app._check_setup_finished = lambda f_name: None

        # remove an existing Blueprint with the given ID
        self.app.blueprints.pop(self.id, None)
        self.app.view_functions.pop(f"{self.id}.api", None)

        return self.app

    def __exit__(self, type, value, traceback):
        # reset app._check_setup_finished, so that Blueprint setupmethods are denied again
        self.app._check_setup_finished = self._check_setup_finished


class Component:
    def __init__(self, path):
        self.path = path
        self.update()

    def update(self):
        self.init_config()
        self.init_module()

    def init_config(self):
        self.config_path = os.path.join(self.path, "config.json")
        with open(self.config_path, "r") as fp:
            self.config = json.load(fp)

    def init_module(self):
        module_path = os.path.join(self.path, "__init__.py")
        if os.path.exists(module_path):
            module_name = self.id
            spec = importlib.util.spec_from_file_location(module_name, module_path)
            self.module = importlib.util.module_from_spec(spec)
            sys.modules[spec.name] = self.module
            spec.loader.exec_module(self.module)
        else:
            self.module = None

    def register_blueprint(self, app):
        self.blueprint = self.module.floe
        with FlaskBlueprintOverwriter(app, self.id):
            app.register_blueprint(self.blueprint, url_prefix=f"/{self.id}")
        app.config[self.id] = self.config

    @property
    def id(self):
        return self.config["id"]

    @property
    def name(self):
        return self.config["name"]

    @property
    def entrypoint(self):
        return f"/{self.id}/"
