import glob
import os
from pathlib import Path

from lib.component import Component


class Registry:
    def __init__(self, path):
        self.path = path

    @property
    def components(self):
        glob_pattern = os.path.join(self.path, "*", "config.json")
        components = []
        for config_path in glob.glob(glob_pattern):
            component_path = Path(config_path).parent
            component = Component(component_path)
            components.append(component)
        return components
