import json
import os
import shutil

PLAYGROUND_PATH = os.path.join("tests", "playground")

LIB_CODE_TPL = """
from flask import Blueprint, current_app

def num():
    return {NUM}

floe = Blueprint("{ID}", __name__)

@floe.route('/test')
def api():

    return {
        "num": num(),
        "config": current_app.config.get("{ID}"),
    }
"""


def init_playground():
    shutil.rmtree(PLAYGROUND_PATH, ignore_errors=True)
    os.mkdir(PLAYGROUND_PATH)
    return PLAYGROUND_PATH


def create_registry(registry_dirname):
    registry_dir = os.path.join(PLAYGROUND_PATH, registry_dirname)
    shutil.rmtree(registry_dir, ignore_errors=True)
    os.mkdir(registry_dir)
    return registry_dir


def create_component(registry_dirname, cfg, *, lib_code=None):
    component_dir = os.path.join(PLAYGROUND_PATH, registry_dirname, cfg["id"])
    shutil.rmtree(component_dir, ignore_errors=True)
    os.mkdir(component_dir)

    with open(os.path.join(component_dir, "config.json"), "w") as fp:
        json.dump(cfg, fp, indent=2)

    if lib_code is not None:
        with open(os.path.join(component_dir, "__init__.py"), "w") as fp:
            fp.write(lib_code)

    return component_dir


def create_lib_code(id, num):
    return LIB_CODE_TPL.replace("{ID}", id).replace("{NUM}", str(num))
