import unittest
import uuid

from lib.component import Component

from .lib import create_lib_code, create_component, create_registry, init_playground


class TestComponent(unittest.TestCase):
    def test(self):
        # create registry with component

        id = str(uuid.uuid4())

        cfg = {
            "id": id,
            "name": "Test component",
        }

        init_playground()
        create_registry("testreg")
        lib_code = create_lib_code(id, 42)
        component_dir = create_component("testreg", cfg, lib_code=lib_code)

        # test

        comp = Component(component_dir)

        self.assertEqual(comp.name, cfg["name"])
        self.assertEqual(comp.module.num(), 42)

        # update config and lib code

        cfg["name"] = "Updated test component"

        lib_code = create_lib_code(id, 43)
        create_component("testreg", cfg, lib_code=lib_code)
        comp.update()

        # test

        self.assertEqual(comp.id, id)
        self.assertEqual(comp.name, cfg["name"])
        self.assertEqual(comp.entrypoint, "/" + id + "/")
        self.assertEqual(comp.module.num(), 43)
