import unittest
import uuid

from lib.registry import Registry

from .lib import init_playground, create_registry, create_component


class TestRegistry(unittest.TestCase):
    def test_empty(self):
        reg = Registry("./does-not-exist/")
        comp = reg.components

        self.assertEqual(len(comp), 0)

    def test_one_component(self):
        cfg = {
            "id": str(uuid.uuid4()),
            "name": "test-component",
        }

        init_playground()
        registry_dir = create_registry("testreg")
        create_component("testreg", cfg)

        # test

        reg = Registry(registry_dir)
        comp = reg.components

        self.assertEqual(len(comp), 1)
