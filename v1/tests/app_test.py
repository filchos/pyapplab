import pytest
import unittest
import uuid

from app import app
from lib.component import Component

from .lib import create_lib_code, create_component, create_registry, init_playground


@pytest.fixture(scope="class")
def client(request):
    app.config["TESTING"] = True

    with app.test_client() as client:
        request.cls.client = client


@pytest.mark.usefixtures("client")
class TestBlueprint(unittest.TestCase):
    def test_component(self):
        id = str(uuid.uuid4())
        cfg = {
            "id": id,
            "name": "Test floe",
        }

        init_playground()
        create_registry("testreg")
        lib_code = create_lib_code(id, 23)
        component_dir = create_component("testreg", cfg, lib_code=lib_code)

        comp = Component(component_dir)
        comp.register_blueprint(app)

        # test

        client = self.client
        result = client.get(f"/{id}/test")
        self.assertEqual(result.status_code, 200)
        j = result.json
        self.assertEqual(j.get("num"), 23)
        self.assertEqual(j.get("config", {}).get("name"), "Test floe")

        # update

        lib_code = create_lib_code(id, 29)
        cfg["name"] = "Updated test floe"
        component_dir = create_component("testreg", cfg, lib_code=lib_code)
        comp.update()
        comp.register_blueprint(app)

        # test

        client = self.client
        result = client.get(f"/{id}/test")
        self.assertEqual(result.status_code, 200)
        j = result.json
        self.assertEqual(j.get("num"), 29)
        self.assertEqual(j.get("config", {}).get("name"), "Updated test floe")
