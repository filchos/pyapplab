import json
import logging
import os
import requests

from lib.app import apps_from_config

CWD = os.path.dirname(os.path.realpath(__file__))

config_path = os.path.join(CWD, "registry.json")
with open(config_path, "r") as fp:
    config = json.load(fp)

logging.basicConfig()
log = logging.getLogger("LAB")
log.setLevel(logging.DEBUG)

apps = apps_from_config(config, CWD, log=log)

apps.cleanup()
apps.build()
apps.start()

for app in apps:
    res = requests.get(app.entrypoint)
    print("Request:", res.content.decode("utf-8"))

apps.stop()
