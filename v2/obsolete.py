import subprocess
import threading

from lib.app import App


class DummyApp(App):
    _running = False

    def start(self):
        self._running = True
        print(f"Started {self.name} on port {self.port}")

    def stop(self):
        self._running = False
        print(f"Stopped {self.name} on port {self.port}")

    @property
    def running(self):
        return self._running


class FlaskApp(App):
    thread = None

    def _start_subprocess(self):
        self.proc = subprocess.run(
            ["flask", "run", "-p", str(self.port)],
            check=True,
            cwd="./apps/app-1",
            env={
                "VIRTUAL_ENV": "/home/olaf/venv/kph",
                "PATH": "/home/olaf/venv/kph/bin:/usr/bin",
            },
            executable="/bin/bash",
            shell=True,
        )
        print(f"Started subprocess on port {self.port}")

    @property
    def running(self):
        return self.thread is not None and self.thread.is_alive()

    def start(self):
        if self.running:
            print("already running")
            return

        def sp():
            self._start_subprocess()

        self.thread = threading.Thread(target=sp)
        self.thread.start()
        print(f"Started {self.name} on port {self.port}")

    def stop(self):
        if self.thread is not None:
            self.thread._stop()
            print(f"Stopped {self.name} on port {self.port}")
