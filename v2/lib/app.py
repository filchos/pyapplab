import abc
import collections
from concurrent.futures import ThreadPoolExecutor
import docker
import os
import time

import logging


class AppCollection(collections.UserList):
    def build(self):
        with ThreadPoolExecutor() as ex:
            for app in self:
                ex.submit(app.build)

    def cleanup(self):
        with ThreadPoolExecutor() as ex:
            for app in self:
                ex.submit(app.cleanup)

    def start(self):
        with ThreadPoolExecutor() as ex:
            for app in self:
                ex.submit(app.start)

    def stop(self):
        with ThreadPoolExecutor() as ex:
            for app in self:
                ex.submit(app.stop)

    def restart(self):
        with ThreadPoolExecutor() as ex:
            for app in self:
                ex.submit(app.restart)


class App:
    def __init__(self, id, port, *, log=None):
        self.id = id
        self.port = port
        if log is None:
            self.log = logging.getLogger("lib.App")
            self.log.addHandler(logging.NullHandler())
        else:
            self.log = log

    def __enter__(self):
        self.cleanup()
        self.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    def __repr__(self):
        return f"<App {self.id} :{self.port}>"

    def build(self):
        pass

    def cleanup(self):
        pass

    @abc.abstractmethod
    def start(self):
        pass

    @abc.abstractmethod
    def stop(self):
        pass

    @property
    @abc.abstractmethod
    def running(self):
        pass

    @property
    def entrypoint(self):
        return f"http://localhost:{self.port}/"

    def restart(self):
        if self.running:
            self.stop()
        self.start()


class DockerApp(App):
    def __init__(self, id, port, *, path, log=None):
        self.path = path
        super().__init__(id, port, log=log)
        self.client = docker.from_env()
        self.container = None

    @property
    def running(self):
        # TODO check that port is accessible
        return self.container is not None

    @property
    def tag(self):
        return "kphlab-" + self.id

    def cleanup(self):
        self.log.info(f"Cleaning up {self.id}")
        for container in self._find_containers():
            if container.status == "running":
                self.log.debug("- kill")
                container.kill()
            self.log.debug("- remove")
            container.remove()
        self.log.info(f"Cleaned up {self.id}")

        self.container = None

    def build(self):
        self.log.info(f"Building {self.id} with tag {self.tag} in {self.path}")
        self.client.images.build(path=self.path, tag=self.tag, rm=True)

    def start(self):
        if self.container is not None:
            self.log.info("Container already started")
            return

        ports = {f"{self.port}/tcp": self.port}
        self.log.info(f"Starting {self.id} on port {self.port}")
        self.container = self.client.containers.run(self.tag, detach=True, ports=ports)

        # TODO: wait for Flask being accessible
        time.sleep(0.5)

        self.log.info(f"Started {self.id} on port {self.port}")

    def stop(self):
        if self.container is None:
            self.log.info("Container not started")
            return

        self.log.info(
            f"Stopping {self.id} on port {self.port} [{self.container.status}]"
        )
        self.container.stop()
        self.log.info(f"Stopped {self.id} on port {self.port}")
        self.container = None

    def _find_containers(self):
        for container in self.client.containers.list(all=True):
            tags = [t.split(":")[0] for t in container.image.attrs["RepoTags"]]
            if self.tag in tags:
                yield container


def apps_from_config(cfg_list, base_dir, *, log=None):
    apps = AppCollection()
    for cfg_item in cfg_list:
        path = os.path.normpath(os.path.join(base_dir, cfg_item["path"]))
        app = DockerApp(cfg_item["id"], cfg_item["port"], path=path, log=log)
        apps.append(app)
    return apps
